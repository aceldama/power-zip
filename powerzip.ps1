[CmdletBinding()]Param(
    [Parameter(Mandatory=$False)][string]$file
)

#----------------------------------------------------------------------------------------------------------------------
function String-Decompress-LZW([byte[]]$arr) {
    function _unpack([ref][byte[]]$arr) {
        $idx = 0
        [int[]]$result = @( 0 ) * 1000

        #-- Get header
        $buff = [convert]::ToString($arr.value[0], 2).PadLeft(8, "0")
        $bits = [convert]::ToByte($buff.SubString(0,6), 2)
        $buff = $buff.SubString(6)

        #-- Unpack bitstream
        write-host "   * Unpacking"
        for ( $i=1; $i -lt $arr.value.length; $i++ ) {
            $buff += [convert]::ToString($arr.value[$i], 2).PadLeft(8, "0")
            while ( $buff.length -ge $bits ) {
                if ( $idx -ge $result.length ) {
                    $result += @( 0 ) * 1000
                }
                $result[$idx] = [convert]::ToInt32($buff.SubString(0, $bits), 2)
                $idx += 1

                if ( $buff.length -gt $bits ) {
                    $buff = $buff.SubString($bits)
                } else {
                    $buff = ""
                }
            }
        }
        write-host ( "     - Waste: {0} bits" -f $buff.length )

        if ( $idx -lt $result.length ) {
            $result = $result[0..($idx - 1)]
        }

        return $result
    }
    
    $result = ""

    #-- Unpack
    [int[]]$arr = _unpack ([ref]$arr)

    #-- Init dictionary
    $dict = @( )
    for ($i = 0; $i -lt 256; $i++) {
        $dict += @( [char]$i )
    }

    #-- Decompress
    $w = ( $dict[[byte]($arr[0])] )
    $entry = ""
    $result = $w
    for ($i=1; $i -lt $arr.length ;$i++) {
        $k = [int]($arr[$i])
        if ( $k -lt $dict.length ) {
            $entry = $dict[$k]
        } else {
            if ( $k -eq $dict.length ) {
                $entry = ( "{0}{1}" -f $w, $w[0] )
            } else {
                #-- Error
                return $null
            }
        }
        $result = ( "{0}{1}" -f $result, $entry )
        $dict += @( "{0}{1}" -f $w, $entry[0] )
        $w = $entry
    }

    return $result
}


function String-Compress-LZW($unc) {
    function _pack([ref][int[]]$arr) {
        #-- Packed:
        #   Header: [XXXXXX][*Data*]
        #       X: bits needed to store bytes
        #   Data: [N * X]
        #       N: the length of the array to be encoded

        $idx = 0
        [byte[]]$result = @( 0 ) * 1000

        write-host "   * Packing"
        write-host "     - Finding largest integer"
        $bits = 1
        for ($i=0; $i -lt $arr.value.length; $i++) {
            while ( [math]::pow(2, $bits) -le $arr.value[$i] ) {
                $bits += 1
            }
        }

        write-host ( "     - Packing at {0} bits" -f $bits )
        $buff = [convert]::ToString($bits, 2).PadLeft(6, "0")   #-- Save size
        for ( $i = 0; $i -lt $arr.value.length; $i++ ) {
            $buff += [convert]::ToString($arr.value[$i], 2).PadLeft($bits, "0")
            while ( $buff.length -ge 8 ) {
                if ( $idx -ge $result.length ) { $result += @( 0 ) * 1000 }
                $result[$idx] = [convert]::ToByte($buff.SubString(0,8), 2)
                $idx += 1
                if ( $buff.length -gt 8) {
                    $buff = $buff.SubString(8)
                } else {
                    $buff = ""
                }
            }
        }
        if ( ![string]::IsNullOrEmpty($buff) ) {
            write-host ( "     - Waste: {0} bits" -f (8 - $buff.length) )
            if ( $idx -ge $result.length ) { $result += @( 0 ) }
            $result += @( [convert]::ToByte($buff.PadRight(8, "0"), 2) )
            $idx += 1
        }

        if ( $idx -lt $result.length ) {
            #-- Trim array
            $result = $result[0..($idx - 1)]
        }
        
        return $result
    }

    $result = ""

    #-- Init dictionary
    $dict = @{ }
    for ($i = 0; $i -lt 256; $i += 1) {
        #-- Save bytes as hex so we have 
        $dict[("{0:x2}" -f $i)] = $i
    }

    #-- Compress
    $w = "00"
    $wc = ""
    [int[]]$arr = @( )
    for ($i = 0; $i -lt $unc.length; $i++) {
        if ( $i -and (($i % 5000) -eq 0) ) {
            #-- Progress
            write-host ( "  - {0:N2}%" -f ($i / $unc.length * 100) )
        }
        
        $wc = ( "{0}{1:x2}" -f $w, [byte][char]($unc[$i]) )
        if ( $dict.Contains($wc) ) {
            $w = $wc
        } else {
            if ( $i -gt 0 ){
                $arr += @( $dict[$w] )
            }
            $dict[$wc] = ($dict.Count - 1)
            $w = ( "{0:x2}" -f [byte][char]($unc[$i]) )
        }
    }
    if ( ![string]::IsNullOrEmpty($w) ) {
        $arr += @( $dict[$w] )
    }

    $result = ( _pack ([ref]$arr) )

    write-host ( "   * Input:  {0}" -f $unc.length )
    write-host ( "   * Output: {0} ({1:N2}% smaller)" -f $result.length, (100 - (100 * $result.length / $unc.length)) )
    return $result
}


#----------------------------------------------------------------------------------------------------------------------
function Print-Time($time=$null) {
    $gd = (Get-Date)
    if ( $time ) {
        write-host ( "* Stop:  {0:hh:mm:ss.FFF} ({1}ms)`n`n" -f $gd, (New-TimeSpan -Start $time -End $gd ).TotalMilliseconds )
    } else {
        write-host ( "* Start: {0:hh:mm:ss.FFF}" -f $gd )
    }
    return $gd
}


function Print-Head($title, $clear="") {
    write-host ("$clear==[ $title ]").PadRight(40, "=")
    return ( Print-Time )
}


function Print-IO($title, $data) {
    $title = ( "{0}:" -f $title ).PadRight(4)
    $max = 12
    if ( $data.length -gt (2 * $max) ) {
        $text = ( "{0} ... {1}" -f (($data[0..($max - 2)] -join ","), ($data[-($max + 1)..-1] -join ",")) )
    } else {
       $text = ( $data -join ", ")
    }
    write-host ( "  $title [{0}] -> {1}" -f $data.length, $text )
}


#----------------------------------------------------------------------------------------------------------------------
if ( $file ) {
    #-- Set input
    $t_start = Print-Head "Reading file"
    $text = ( GC $file -raw )
    $t_start = Print-Time $t_start

    $t_start = Print-Head "LZW zip"
    $output = (String-Compress-LZW $input)
    $t_start = Print-Time $t_start

    $t_start = Print-Head "LZW unzip"
    $output = (String-Decompress-LZW $output)
    Print-IO "IN"  ($input)
    Print-IO "OUT" ([text.encoding]::UTF8.GetBytes($output))
    $t_start = Print-Time $t_start
} else {
    Write-Host ( "Please specify a file with the -file parameter.`n`n" )
}